'use strict'

const Project = use('App/Models/Project')
const Tag = use('App/Models/Tag')
const Drive = use('Drive')
const Env = use('Env')
const Helpers = use('Helpers')
const Upload = use('App/Tools/Upload')
const { validate } = use('Validator')

class PortfolioController {
  async index ({response}) {
    const projects = await Project
      .query()
      .with('tags')
      .fetch()
    return response.status(200).json(projects)
  }

  async show ({params, response}) {
    const project = await Project
      .query()
      .with('tags')
      .where('slug', params.slug)
      .first()

    if (project) {
      return response.status(200).json(project)
    } else {
      return response.status(404).json('not found')
    }
  }

  async store ({request, response}) {
    let errors = []
    let { title, draft, content, website, repository, tags, extra } = request.all()
    draft = (draft === '1')
    let illustration = ''
    let images = ''
    let thumbnail = ''
    const project = await Project.create({
      title,
      draft,
      content,
      website,
      repository,
      illustration,
      images,
      thumbnail
    })

    tags = tags.split(',').map(Number)
    if (tags && tags.length > 0) {
      await project.tags().attach(tags)
    }

    // handling illustration
    const single = request.file('illustration', {
      types: ['image'],
      extnames: ['png', 'jpg', 'jpeg']
    })
    if (single) {
      let {illustration, thumbnail} = await Upload.storeProject(project.id, single)
      project.illustration = illustration
      project.thumbnail = thumbnail

    } else {
      errors.push({message: 'An illustration is required'})
    }

    // Handling multi, except in testing phases
    // Multi = added images next to the project for illustration purposes.
    if (Env.get('NODE_ENV') !== 'testing') {
      const multi = request.file('images', {
        types: ['image'],
        extnames: ['png', 'jpg', 'jpeg']
      })
      project.images = await Upload.storeMultiProject(project.id, multi)
    }

    // Handling extras
    // Extras are the images inside the project's description if any
    if (extra) {
      project.content = await Upload.storeExtraProject(extra, project.id, project.content)
    }

    if (errors.length > 0) {
      return response.status(401).json(errors)
    }

    await project.save()
    return response.status(201).json(project)
  }

  async update ({params, request, response}) {
    const project = await Project.find(params.id)
    let errors = []
    const data = request.all()
    data.thumbnail = ''
    let tags = data.tags
    delete data.tags

    // handling illustration
    const single = request.file('illustration', {
      types: ['image'],
      extnames: ['png', 'jpg', 'jpeg']
    })

    if (single) {
      const old = project.illustration.split(/[\\/]/).pop()
      const {illustration, thumbnail} = await Upload.updateProject(old, project.id, single)
      data.illustration = illustration
      data.thumbnail = thumbnail
    } else {
      data.illustration = project.illustration
      data.thumbnail = project.thumbnail
    }
    // Handling multi
    const multi = request.file('images', {
      types: ['image'],
      extnames: ['png', 'jpg', 'jpeg']
    })
    if (multi) {
      data.images = await Upload.updateMultiProject(project.id, multi)
    }

    if (data.extra) {
      project.content = await Upload.storeExtraProject(data.extra, project.id, project.content)
    }

    if (errors.length > 0) {
      return response.status(401).json(errors)
    }

    ['title', 'illustration', 'draft', 'thumbnail', 'content', 'images', 'website', 'repository'].map(field => {
      project[field] = data[field] || project[field]
    })

    await project.save()
    await project.tags().detach()
    await project.tags().attach(tags)
    return response.status(200).json(project)
  }

  async upload ({request, response}) {
    const file = request.file('file', {
      type: ['image']
    })

    const { id } = request.all()
    // available types: multi|extra
    if (id) {
      await file.move(Helpers.publicPath(`project/${id}`), {
        overwrite: true,
        name: file.clientName.replace(/ /gm, '-')
      })
    }
    await file.move(Helpers.publicPath('projects/tmp'), {
      overwrite: true,
      name: file.clientName.replace(/ /gm, '-')
    })

    if (!file.moved()) {
      return response.status(401).json(file.errors())
    } else {
      return response.status(200).json(`${Env.get('APP_URL')}/projects/tmp/${file.clientName}`)
    }
  }

  async deleteUploads ({request, response}) {
    const validation = await validate(request.all(), {
      urls: 'required'
    })

    if (validation.fails()) {
      return response.status(401).json(validation.messages())
    } else {
      const { urls } = request.only(['urls'])
      for (const url in urls) {
        await Drive.delete(url)
      }

      return response.status(200).json('ok')
    }
  }

  async delete ({params, response}) {
    const project = await Project.find(params.id)
    if (!project) {
      return response.status(404).json(null)
    } else {
      await project.tags().detach()
      await project.delete()
      await Drive.delete(Helpers.publicPath(`projects/${params.id}`))
      return response.status(200).json('ok')
    }
  }

  async publish ({request, response}) {
    const { id } = request.only(['id'])
    const project = await Project.find(id)
    project.draft = !project.draft
    await project.save()

    return response.status(200).json(project)
  }

  async related ({request, response}) {
    const validation = await validate(request.all(), {
      tag_id: 'integer|required',
      project_id: 'integer|required'
    })

    if (validation.fails()) {
      return response.status(401).json(validation.messages())
    } else {
      const { tag_id, project_id } = request.all()
      const tag = await Tag.find(tag_id)
      const projects = await tag
        .project()
        .where('draft', false)
        .whereNot('id', project_id)
        .fetch()
      return response.status(200).json(projects)
    }
  }
}

module.exports = PortfolioController
