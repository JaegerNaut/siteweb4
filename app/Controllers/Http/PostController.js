'use strict'

const Post = use('App/Models/Post')
const Tag = use('App/Models/Tag')
const Upload = use('App/Tools/Upload')
const Helpers = use('Helpers')
const Drive = use('Drive')
const { validateAll, validate } = use('Validator')

class PostController {
  async index ({ response }) {
    const posts = await Post
      .query()
      .with('tags')
      .fetch()
    return response.status(200).json(posts)
  }

  async infinite ({request, response}) {
    const { page, limit, locale } = request.all()
    const posts = await Post
      .query()
      .where('lang', locale)
      .with('tags')
      .paginate(page, limit)
    return response.status(200).json(posts)
  }

  async show ({params, response}) {
    const post = await Post
      .query()
      .with('tags')
      .where('slug', params.slug)
      .first()

    if (post) {
      post.tags = await post.tags().fetch()
      return response.status(200).send(post)
    } else {
      return response.status(404).send(null)
    }
  }

  async store ({request, response}) {
    let illustration = ''
    const image = request.file('illustration', {
      types: ['image'],
      allowedExtensions: ['jpg', 'png', 'jpeg'],
      size: '10mb'
    })

    let {title, content, lang, draft} = request.all()
    let thumbnail = ''
    draft = (draft === '1')
    const estimate = Math.ceil(content.split(" ").length / 200)
    console.log(request.all())
    const post = await Post.create({title, estimate, content, thumbnail, lang, draft, illustration})
    console.log('test')
    const tags = request.input('tags').split(',').map(Number)
    if (tags && tags.length > 0) {
      await post.tags().attach(tags)
    }

    if (image) {
      const {illustration, thumbnail} = await Upload.storePost(post.id, image)
      post.illustration = illustration
      post.thumbnail = thumbnail

      await post.save()
    }

    return response.status(201).json(post)
  }

  async update ({params, request, response}) {
    const post = await Post.query().where('slug', params.slug).first()
    const { title, content, draft, lang } = request.all()
    const tags = request.input('tags').split(',').map(Number)
    post.title = title || post.title
    post.content = content || post.content
    post.lang = lang || post.lang
    post.draft = (draft === '1') || post.draft

    const image = request.file('illustration', {
      types: ['image'],
      allowedExtensions: ['jpg', 'png', 'jpeg'],
      size: '10mb'
    })

    if (image) {
      let {illustration, thumbnail} = await Upload.updatePost(post.id, image)
      post.illustration = illustration
      post.thumbnail = thumbnail
    }

    await post.save()

    await post.tags().detach()
    await post.tags().attach(tags)

    return response.status(200).json(post)
  }

  async delete ({params, response}) {
    const post = await Post.find(params.id)
    if (!post) {
      return response.status(404).json(null)
    } else {
      await post.tags().detach()
      await Drive.delete(Helpers.publicPath(`articles/${params.id}`))
      await post.delete()
      return response.status(200).json('ok')
    }
  }

  async publish ({request, response}) {
    const { id } = request.only(['id'])
    const post = await Post.find(id)
    post.draft = !post.draft
    await post.save()
    return response.status(200).json(post)
  }

  async related ({request, response}) {
    const validation = await validate(request.all(), {
      postId: 'integer|required',
      tagId: 'integer|required'
    })

    if (validation.fails()) {
      return response.status(401).json(validation.messages())
    } else {
      const {postId, tagId} = request.all()
      const tag = await Tag.find(tagId)
      const posts = await tag
        .posts()
        .where('draft', false)
        .whereNot('id', postId)
        .limit(4)
        .fetch()
      return response.status(200).json(posts)
    }
  }
}

module.exports = PostController
