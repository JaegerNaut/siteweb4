'use strict'

const Helpers = use('Helpers')
const Drive = use('Drive')
const Env = use('Env')
const Jimp = require('jimp')

class Upload {
  /**
   * Handles uploads from PostController
   * @param id
   * @param image
   * @returns {Promise<{thumbnail: string, illustration: string}>}
   */
  static async storePost (id, image) {
    let thumbnail = ''
    await image.move(Helpers.publicPath('articles/' + id), {
      name: image.clientName.replace(/ /gm, '-')
    })
    const path = `articles/${id}/${image.clientName}`
    const illustration = `${Env.get('APP_URL')}/${path}`
    // Creates a thumbnail only if there's an image.
    // "await image.move()" makes sure there is one.
    if (Drive.exists(Helpers.publicPath(path))) {
      this.postThumbnail(id, path, image.subtype)
      thumbnail = `${Env.get('APP_URL')}/articles/${id}/thumb.${image.subtype}`
    }
    return {illustration, thumbnail}
  }

  /**
   * Deletes the article's directory, then will trigger storePost()
   * @param id
   * @param image
   * @returns {Promise<{thumbnail: string, illustration: string}>}
   */
  static async updatePost (id, image) {
    // Delete the directory's content before uploading the new image
    await Drive.delete(Helpers.publicPath(`articles/${id}`))
    return this.storePost(id, image)
  }

  /**
   * Handles a project's illustration
   * @param id
   * @param image
   * @return {Promise<{thumbnail: string, illustration: string}>}
   */
  static async storeProject (id, image) {
    await image.move(Helpers.publicPath(`projects/${id}`), {
      name: image.clientName.replace(/ /gm, '-'),
      overwrite: true
    })
    const path = `projects/${id}/${image.clientName.replace(/ /gm, '-')}`
    const illustration = `${Env.get('APP_URL')}/${path}`
    if (Drive.exists(Helpers.publicPath(path))) {
      this.projectThumbnail(id, path, image.subtype)
      const thumbnail = JSON.stringify({
        small: `${Env.get('APP_URL')}/projects/${id}/small.${image.subtype}`,
        wide: `${Env.get('APP_URL')}/projects/${id}/wide.${image.subtype}`
      })
      return {illustration, thumbnail}
    }
  }

  /**
   * Handle a project's extra images
   * @param id
   * @param images
   * @return {Promise<string>}
   */
  static async storeMultiProject (id, images) {
    const path = `projects/${id}/images`
    let result = '[]'
    if (images) {
      await images.moveAll(Helpers.publicPath(path))
      if (!images.movedAll()) {
        images.errors().map(error => {
          console.log(error)
        })
      } else {
        result = JSON.stringify(
          images.movedList().map(
            image => {
              return `${Env.get('APP_URL')}/${path}/${image.clientName}`
            }
          )
        )
      }
    }

    return result
  }

  /**
   * Returns the project's content with all its images's path renamed
   * @param extra
   * @param id
   * @param content
   * @return {Promise<string>}
   */
  static async storeExtraProject (extra, id, content) {
    let result = ''
    if (typeof extra === 'string') {
      if (content.includes(extra)) {
        const basename = extra.split(/[\\/]/).pop()
        await Drive.move(Helpers.publicPath(`projects/tmp/${basename}`),
          Helpers.publicPath(`projects/${id}/extra/${basename.replace(/ /gm, '-')}`))

        result = content.replace(/\/tmp\//g, `/${id}/extra/`)
      }
    } else {
      for (let i = 0; i < extra.length; i++) {
        if (content.includes(extra[i])) {
          const basename = extra[i].split(/[\\/]/).pop()
          await Drive.move(Helpers.publicPath(`projects/tmp/${basename}`),
            Helpers.publicPath(`projects/${id}/extra/${basename.replace(/ /gm, '-')}`))
        }
      }
      result = content.replace(/\/tmp\//g, `/${id}/extra/`)
      await Drive.delete(Helpers.publicPath('projects/tmp'))
    }

    return result
  }

  /**
   * Handles a project's illustration edition
   * @param old
   * @param id
   * @param image
   * @return {Promise<{thumbnail: string, illustration: string}>}
   */
  static async updateProject (old, id, image) {
    await Drive.delete(Helpers.publicPath(`projects/${id}/${old}`))
    const unused = image.subtype === 'png' ? 'jpeg' : 'png'
    // Deleting unused thumbnails that won't be overwritten
    if (Drive.exists(Helpers.publicPath(`projects/${id}/small.${unused}`))) {
      await Drive.delete(Helpers.publicPath(`projects/${id}/small.${unused}`))
      await Drive.delete(Helpers.publicPath(`projects/${id}/wide.${unused}`))
    }
    return await this.storeProject(id, image)
  }

  static async updateMultiProject (id, images) {
    await Drive.delete(Helpers.publicPath(`projects/${id}/images`))
    return await this.storeMultiProject(id, images)
  }

  /* ===== THUMBNAILS GENERATION ===== */

  /**
   * Generates a thumbnail for the article page
   * @param id
   * @param path
   * @param subtype
   */
  static postThumbnail (id, path, subtype) {

    Jimp.read(Helpers.publicPath(path))
      .then(image => {
        image
          .scaleToFit(960, 600)
          .crop(240, 150, 384, 240)
          .write(Helpers.publicPath(`articles/${id}/thumb.${subtype}`))
      })

  }

  /**
   * Generates 2 thumbnails for the portfolio page
   * @param id
   * @param path
   * @param subtype
   */
  static projectThumbnail (id, path, subtype) {

    Jimp.read(Helpers.publicPath(path))
      .then(image => {
        image
          .scaleToFit(960, 600)
          .crop(240, 150, 384, 240)
          .write(Helpers.publicPath(`projects/${id}/small.${subtype}`))
      })

    Jimp.read(Helpers.publicPath(path))
      .then(image => {
        image
          .scaleToFit(960, 600)
          .crop(96, 150, 768, 240)
          .write(Helpers.publicPath(`projects/${id}/wide.${subtype}`))
      })
  }
}

module.exports = Upload
