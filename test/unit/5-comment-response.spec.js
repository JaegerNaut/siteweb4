'use strict'

const { test, trait } = use('Test/Suite')('Comment')

trait('Test/ApiClient')

test('Create a comment', async ({ client }) => {
  const data = {
    email: 'test@test.test',
    name: 'test',
    comment: 'Donec eget facilisis dui, eget sagittis turpis. Fusce sollicitudin nulla turpis, sed ornare nibh maximus sed',
    accepted: false,
    reply: 1,
    post_id: 1
  }

  const response = await client
    .post('/api/v1/comment')
    .send(data)
    .end()

  response.assertStatus(201)
  response.assertJSONSubset({
    email: 'test@test.test',
    name: 'test',
    comment: 'Donec eget facilisis dui, eget sagittis turpis. Fusce sollicitudin nulla turpis, sed ornare nibh maximus sed',
    accepted: false,
    reply: 1,
    post_id: 1
  })
})
