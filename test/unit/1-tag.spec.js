'use strict'

const { test, trait } = use('Test/Suite')('Tag')
const User = use('App/Models/User')

trait('Test/ApiClient')
trait('Auth/Client')

test('Create a test tag', async ({ client }) => {
  const user = await User.first()
  const data = {
    name_fr: 'test',
    name_en: 'test',
    icon: 'fab fa-php',
    description_fr: 'lorem ipsum sic amet',
    description_en: 'lorem ipsum sic amet'
  }

  const response = await client
    .post('/api/v1/tag')
    .loginVia(user, 'jwt')
    .send(data)
    .end()

  response.assertStatus(201)
  response.assertJSONSubset({
    name_fr: 'test',
    name_en: 'test',
    icon: 'fab fa-php',
    description_fr: 'lorem ipsum sic amet',
    description_en: 'lorem ipsum sic amet',
    slug: 'test'
  })
})
