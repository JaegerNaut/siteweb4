'use strict'

const { test, trait } = use('Test/Suite')('Delete All')
const User = use('App/Models/User')
const Comment = use('App/Models/Comment')

trait('Test/ApiClient')
trait('Auth/Client')

test("Delete a comment's comment", async ({ client }) => {
  const user = await User.first()
  const comment = await Comment.findBy('reply', 1)
  const response = await client
    .delete(`/api/v1/comment/delete/${comment.id}`)
    .loginVia(user, 'jwt')
    .end()

  response.assertStatus(200)
})

test('Delete a comment', async ({ client }) => {
  const user = await User.first()
  const response = await client
    .delete('/api/v1/comment/delete/1')
    .loginVia(user, 'jwt')
    .end()

  response.assertStatus(200)
})

test('Delete a project', async ({ client }) => {
  const user = await User.first()
  const response = await client
    .delete('/api/v1/portfolio/1')
    .loginVia(user, 'jwt')
    .end()

  response.assertStatus(200)
})

test('Delete a post', async ({ client }) => {
  const user = await User.first()
  const response = await client
    .delete('/api/v1/post/1')
    .loginVia(user, 'jwt')
    .end()

  response.assertStatus(200)
})

test('Delete a tag', async ({ client }) => {
  const user = await User.first()
  const response = await client
    .delete('/api/v1/tag/1')
    .loginVia(user, 'jwt')
    .end()

  response.assertStatus(200)
})
