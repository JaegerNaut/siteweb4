'use strict'

const { test, trait } = use('Test/Suite')('Project')
const User = use('App/Models/User')
const Tag = use('App/Models/Tag')
const Helpers = use('Helpers')
const Env = use('Env')

trait('Test/ApiClient')
trait('Auth/Client')

test('Create a project', async ({ client }) => {
  const user = await User.first()
  const tag = await Tag.first()

  const response = await client
    .post('/api/v1/portfolio/store')
    .loginVia(user, 'jwt')
    .field('title', 'project test')
    .field('content', 'Lorem ipsum dolor sit amet posuere.')
    .field('website', 'https://www.google.fr')
    .field('repository', 'https://www.google.fr')
    .field('draft', '1')
    .field('lang', 'en')
    .field('tags', tag.id)
    .field('images', '')
    .attach('illustration', Helpers.resourcesPath('test/jo.png'))
    .attach('thumbnail', Helpers.resourcesPath('test/jo.png'))
    .end()

  response.assertStatus(201)
  response.assertJSONSubset({
    title: 'project test',
    illustration: `${Env.get('APP_URL')}/projects/1/jo.png`,
    thumbnail: `{"small":"${Env.get('APP_URL')}/projects/1/small.png","wide":"${Env.get('APP_URL')}/projects/1/wide.png"}`,
    draft: true,
    content: 'Lorem ipsum dolor sit amet posuere.',
    images: '',
    website: 'https://www.google.fr',
    repository: 'https://www.google.fr',
    slug: 'project-test'
  })
})
