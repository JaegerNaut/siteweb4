'use strict'

const { test, trait } = use('Test/Suite')('Post')
const Env = use('Env')
const User = use('App/Models/User')
const Tag = use('App/Models/Tag')
const Helpers = use('Helpers')

trait('Test/ApiClient')
trait('Auth/Client')

test('Create a post', async ({ client }) => {
  const user = await User.first()
  const tag = await Tag.first()

  const response = await client
    .post('/api/v1/post')
    .loginVia(user, 'jwt')
    .field('title', 'post test')
    .field('content', 'Lorem ipsum dolor sit amet posuere.')
    .field('draft', '1')
    .field('lang', 'en')
    .field('tags', tag.id)
    .attach('illustration', Helpers.resourcesPath('test/jo.png'))
    .attach('thumbnail', Helpers.resourcesPath('test/jo.png'))
    .end()

  response.assertStatus(201)
  response.assertJSONSubset({
    title: 'post test',
    content: 'Lorem ipsum dolor sit amet posuere.',
    illustration: `${Env.get('APP_URL')}/articles/1/jo.png`,
    thumbnail: `${Env.get('APP_URL')}/articles/1/thumb.png`,
    estimate: 1,
    lang: 'en',
    slug: 'post-test'
  })
})
